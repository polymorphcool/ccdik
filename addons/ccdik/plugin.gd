tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("CCDIK_Solver", "Node", preload("ccdik_solver.gd"), preload("ccdik_solver.svg"))
	add_custom_type("CCDIK_Chain", "Node", preload("ccdik_chain.gd"), preload("ccdik_chain.svg"))
	add_custom_type("CCDIK_Constraint", "Node", preload("ccdik_constraint.gd"), preload("ccdik_constraint.svg"))

func _exit_tree():
	remove_custom_type("CCDIK_Solver")
	remove_custom_type("CCDIK_Chain")
	remove_custom_type("CCDIK_Constraint")
